﻿namespace EscolaMVC.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class ProfessorDisciplinaModels
    {
        [Key]
        public int ProfessorDisciplinaid { get; set; }

        public int ProfessorId { get; set; }

        public int DisciplinaId { get; set; }

        [ForeignKey("ProfessorId")]
        public ProfessorViewModels Professor { get; set; }

        [ForeignKey("DisciplinaId")]
        public DisciplinaViewModels Disciplina { get; set; }


    }
}