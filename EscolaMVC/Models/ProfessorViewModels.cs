﻿namespace EscolaMVC.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ProfessorViewModels
    {
        [Key]
        public int ProfessorId { get; set; }

        public int CargoId { get; set; }

        [ForeignKey("CargoId")]
        public CargoViewModels Cargo { get; set; }
    }
}