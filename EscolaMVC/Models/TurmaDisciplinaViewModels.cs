﻿namespace EscolaMVC.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class TurmaDisciplinaViewModels
    {
        [Key]
        public int TurmaDisciplinaId { get; set; }

        [DisplayName("Turma")]
        public int TurmaId { get; set; }

        [DisplayName("Disciplina")]
        public int DisciplinaId { get; set; }

        [ForeignKey("TurmaId")]
        public TurmaViewModels Turma { get; set; }

        [ForeignKey("DisciplinaId")]
        public DisciplinaViewModels Disciplina { get; set; }
    }
}