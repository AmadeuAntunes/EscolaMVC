﻿namespace EscolaMVC.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class AlunoDisciplinaNota
    {
        [Key]
        public int AlunoDisciplinaNotaId { get; set; }

        public int UtilizadorId { get; set; }

        public int DisciplinaId { get; set; }

        public double Nota { get; set; }

        [ForeignKey("UtilizadorId")]
        public UtilizadorViewModels Utilizador { get; set; }

        [ForeignKey("DisciplinaId")]
        public DisciplinaViewModels Disciplina { get; set; }


    }
}