﻿namespace EscolaMVC.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class CursoDisciplinaViewModels
    {
        [Key]
        public int CursoDisciplinaId { get; set; }

        [DisplayName("Curso")]
        public int CursoId { get; set; }

        [DisplayName("Disciplina")]
        public int DisciplinaId { get; set; }

        [ForeignKey("CursoId")]
        public CursoViewModels Curso { get; set; }

        [ForeignKey("DisciplinaId")]
        public DisciplinaViewModels Disciplina { get; set; }

    }

}