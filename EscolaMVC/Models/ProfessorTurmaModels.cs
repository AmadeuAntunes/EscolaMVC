﻿namespace EscolaMVC.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class ProfessorTurmaModels
    {
        [Key]
        public int ProfessorTurmaId { get; set; }

        public int TurmaId { get; set; }

        public int ProfessorId { get; set; }

        [ForeignKey("TurmaId")]
        public TurmaViewModels Turma { get; set; }

        [ForeignKey("ProfessorId")]
        public ProfessorViewModels Professor { get; set; }
    }
}