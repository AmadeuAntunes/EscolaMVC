﻿namespace EscolaMVC.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class NotaViewModels
    {
        [Key]
        public int NotaId { get; set; }

        [Required]
        public int CursoId { get; set; }

        [Required]
        public int TurmaId { get; set; }

        [Required]
        public int DisciplinaId { get; set; }

        [Required]
        public int UtilizadorId { get; set; }

        [DisplayName("Nota")]
        [Required]
        [Range(0,20,ErrorMessage ="Introuza um numero de 0 a 20")]
        public double NotaValor { get; set; }

        [ForeignKey("CursoId")]
        public CursoViewModels Curso { get; set; }

        [ForeignKey("TurmaId")]
        public TurmaViewModels Turma { get; set; }

        [ForeignKey("DisciplinaId")]
        public DisciplinaViewModels Disciplina { get; set; }

        [ForeignKey("UtilizadorId")]
        public UtilizadorViewModels Aluno { get; set; }

    }
}