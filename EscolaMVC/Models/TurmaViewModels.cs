﻿namespace EscolaMVC.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class TurmaViewModels
    {
        [Key]
        public int TurmaId { get; set; }

        [Required(ErrorMessage = "Preencha o Nome da Turma")]
        [DisplayName("Nome da Turma")]
        public string NomeTurma { get; set; }

        public int CursoId { get; set; }

        [ForeignKey("CursoId")]
        public CursoViewModels Curso { get; set; }
       

       

        
    }
}