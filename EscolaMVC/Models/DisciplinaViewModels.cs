﻿namespace EscolaMVC.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    public class DisciplinaViewModels
    {
        [DisplayName("Disciplina")]
        [Key]
        public int DisciplinaId { get; set; }

        [Required(ErrorMessage = "Preencha o Nome da Disciplina")]
        [DisplayName("Nome da Disciplina")]
        public string NomeDisciplina { get; set; }

        [Required(ErrorMessage = "Preencha a Descrição da Disciplina")]
        [DisplayName("Descrição da Disciplina")]
        public string  DescricaoDisciplina{ get; set; }

    }
}