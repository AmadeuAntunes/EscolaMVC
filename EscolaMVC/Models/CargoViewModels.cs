﻿namespace EscolaMVC.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    /*
     1- Admin
     2- Professor 
     3- Aluno
     */
    public class CargoViewModels
    {
        [Key]
        [DisplayName("Cargo")]
        public int CargoId { get; set; }

        [DisplayName("Cargo")]
        public string CargoNome { get; set; }

    }
}