﻿namespace EscolaMVC.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class UtilizadorViewModels
    {
        
        [Key]
        public int UtilizadorId { get; set; }


        [EmailAddress]
        [DisplayName("E-mail")]
        [Required(ErrorMessage = "Preencha o campo E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Preencha o campo Nome")]
        public string Nome { get; set; }



        [Required(ErrorMessage = "Preencha o campo Apelido")]
        public string Apelido { get; set; }

        
        [Required]
        public int CargoId { get; set; }

        [ForeignKey("CargoId")]
        public virtual CargoViewModels Cargo { get; set; }


    }
   
}