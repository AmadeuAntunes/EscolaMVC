﻿namespace EscolaMVC.Models
{
    using System.Configuration;
    using System.Data.Entity;
    public class MyContext : DbContext
    {
      
       
        public DbSet<ProfessorViewModels> Professor { get; set; }
        public DbSet<NotaViewModels> Nota { get; set; }
        public DbSet<DisciplinaViewModels> Disciplina{ get; set; }
        public DbSet<ProfessorDisciplinaModels> ProfessorDisciplina {get; set;}
        public DbSet<TurmaViewModels> Turma { get; set; }
        public DbSet<TurmaAlunoViewModels> TurmaAluno { get; set; }
        public DbSet<ProfessorTurmaModels> ProfessorTurma { get; set; }
        public DbSet<TurmaDisciplinaViewModels> TurmaDisciplina { get; set; }
        public DbSet<CursoViewModels> Curso { get; set; }
        public DbSet<CargoViewModels> Cargo { get; set; }
        public DbSet<UtilizadorViewModels> Utilizador { get; set; }
        public DbSet<TurmaCursoViewModels> TurmaCurso { get; set; }
        public DbSet<CursoDisciplinaViewModels> CursoDisciplina { get; set; }
        public DbSet<AlunoDisciplinaNota> AlunoDisciplinaNotas { get; set; }
       

        public MyContext() : base(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
        {
           
          
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<TurmaViewModels>()
           .HasRequired<CursoViewModels>(s => s.Curso)
           .WithMany(g => g.Turmas)
           .HasForeignKey<int>(s => s.CursoId)
           .WillCascadeOnDelete(false);
        

        }

       
    }



}
