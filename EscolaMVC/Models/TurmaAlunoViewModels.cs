﻿namespace EscolaMVC.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public class TurmaAlunoViewModels
    {
        [Key]
        public int TurmaAlunoid { get; set; }

        public int TurmaId { get; set; }

        public int UtilizadorId { get; set; }

        [ForeignKey("TurmaId")]
        public TurmaViewModels Turma { get; set; }

        [ForeignKey("UtilizadorId")]
        public UtilizadorViewModels Aluno { get; set; }
    }
}