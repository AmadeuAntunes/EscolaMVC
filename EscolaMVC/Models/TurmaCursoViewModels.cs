﻿namespace EscolaMVC.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class TurmaCursoViewModels
    {
        [Key]
        public int CursoTurmaId { get; set; }

        [DisplayName("Turma")]
        public int TurmaId { get; set; }

        [DisplayName("Curso")]
        public int CursoId { get; set; }

        [ForeignKey("TurmaId")]
        public TurmaViewModels Turma { get; set; }

        [ForeignKey("CursoId")]
        public CursoViewModels Curso{ get; set; }
    }
}