﻿namespace EscolaMVC.Models
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    public class CursoViewModels
    {
        [DisplayName("Curso")]
        [Key]
        public int CursoId { get; set; }

        [DisplayName("Nome do Curso")]
        [Required(ErrorMessage = "Preencha o Nome do Curso")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "O nome do curso deve ter entre 3 a 100 caracteres")]
        public string NomeCurso { get; set; }

        public ICollection<TurmaViewModels> Turmas { get; set; }


    }
}