namespace EscolaMVC.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EscolaMVC.Models.MyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Models.MyContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Cargo.AddOrUpdate(
                p => p.CargoId,
                new Models.CargoViewModels { CargoId = 1, CargoNome = "Administrador" },
                new Models.CargoViewModels { CargoId = 2, CargoNome = "Professor" },
                new Models.CargoViewModels { CargoId = 3, CargoNome = "Aluno" },
                new Models.CargoViewModels { CargoId = 4, CargoNome = "N�o atribuido" }
                );

            context.SaveChanges();

        }
    }
}
