namespace EscolaMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate1 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.UtilizadorViewModels", "TurmaId");
            AddForeignKey("dbo.UtilizadorViewModels", "TurmaId", "dbo.TurmaViewModels", "TurmaId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UtilizadorViewModels", "TurmaId", "dbo.TurmaViewModels");
            DropIndex("dbo.UtilizadorViewModels", new[] { "TurmaId" });
        }
    }
}
