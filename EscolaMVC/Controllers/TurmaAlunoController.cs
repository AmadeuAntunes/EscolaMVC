﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EscolaMVC.Models;

namespace EscolaMVC.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class TurmaAlunoController : Controller
    {
        private MyContext db = new MyContext();

        [Authorize]
        // GET: TurmaAluno
        public ActionResult Index()
        {
            var turmaAluno = db.TurmaAluno.Include(t => t.Aluno).Include(t => t.Turma);
            return View(turmaAluno.ToList());
        }

        // GET: TurmaAluno/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TurmaAlunoViewModels turmaAlunoViewModels = db.TurmaAluno.Find(id);
            if (turmaAlunoViewModels == null)
            {
                return HttpNotFound();
            }
            return View(turmaAlunoViewModels);
        }

        // GET: TurmaAluno/Create
        public ActionResult Create()
        {
            ViewBag.UtilizadorId = new SelectList(db.Utilizador.Where(x => x.CargoId == 3), "UtilizadorId", "Email");
            ViewBag.TurmaId = new SelectList(db.Turma, "TurmaId", "NomeTurma");
            return View();
        }

        // POST: TurmaAluno/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TurmaAlunoid,TurmaId,UtilizadorId")] TurmaAlunoViewModels turmaAlunoViewModels)
        {
            if (ModelState.IsValid)
            {
                db.TurmaAluno.Add(turmaAlunoViewModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UtilizadorId = new SelectList(db.Utilizador, "UtilizadorId", "Email", turmaAlunoViewModels.UtilizadorId);
            ViewBag.TurmaId = new SelectList(db.Turma, "TurmaId", "NomeTurma", turmaAlunoViewModels.TurmaId);
            return View(turmaAlunoViewModels);
        }

        // GET: TurmaAluno/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TurmaAlunoViewModels turmaAlunoViewModels = db.TurmaAluno.Find(id);
            if (turmaAlunoViewModels == null)
            {
                return HttpNotFound();
            }
            ViewBag.UtilizadorId = new SelectList(db.Utilizador.Where(x => x.CargoId == 3), "UtilizadorId", "Email", turmaAlunoViewModels.UtilizadorId);
            ViewBag.TurmaId = new SelectList(db.Turma, "TurmaId", "NomeTurma", turmaAlunoViewModels.TurmaId);
            return View(turmaAlunoViewModels);
        }

        // POST: TurmaAluno/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TurmaAlunoid,TurmaId,UtilizadorId")] TurmaAlunoViewModels turmaAlunoViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(turmaAlunoViewModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UtilizadorId = new SelectList(db.Utilizador, "UtilizadorId", "Email", turmaAlunoViewModels.UtilizadorId);
            ViewBag.TurmaId = new SelectList(db.Turma, "TurmaId", "NomeTurma", turmaAlunoViewModels.TurmaId);
            return View(turmaAlunoViewModels);
        }

        // GET: TurmaAluno/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TurmaAlunoViewModels turmaAlunoViewModels = db.TurmaAluno.Find(id);
            if (turmaAlunoViewModels == null)
            {
                return HttpNotFound();
            }
            return View(turmaAlunoViewModels);
        }

        // POST: TurmaAluno/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TurmaAlunoViewModels turmaAlunoViewModels = db.TurmaAluno.Find(id);
            db.TurmaAluno.Remove(turmaAlunoViewModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
