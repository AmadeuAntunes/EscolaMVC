﻿namespace EscolaMVC.Controllers
{
    using EscolaMVC.Models;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    [Authorize(Roles = "Administrador")]
    public class DisciplinaController : Controller
    {
        private MyContext db = new MyContext();

        // GET: Disciplina
        public ActionResult Index()
        {
            return View(db.Disciplina.ToList());
        }

        // GET: Disciplina/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DisciplinaViewModels disciplinaViewModels = db.Disciplina.Find(id);
            if (disciplinaViewModels == null)
            {
                return HttpNotFound();
            }
            return View(disciplinaViewModels);
        }

        // GET: Disciplina/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Disciplina/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DisciplinaId,NomeDisciplina,DescricaoDisciplina")] DisciplinaViewModels disciplinaViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Disciplina.Add(disciplinaViewModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(disciplinaViewModels);
        }

        // GET: Disciplina/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DisciplinaViewModels disciplinaViewModels = db.Disciplina.Find(id);
            if (disciplinaViewModels == null)
            {
                return HttpNotFound();
            }
            return View(disciplinaViewModels);
        }

        // POST: Disciplina/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DisciplinaId,NomeDisciplina,DescricaoDisciplina")] DisciplinaViewModels disciplinaViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(disciplinaViewModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(disciplinaViewModels);
        }

        // GET: Disciplina/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DisciplinaViewModels disciplinaViewModels = db.Disciplina.Find(id);
            if (disciplinaViewModels == null)
            {
                return HttpNotFound();
            }
            return View(disciplinaViewModels);
        }

        // POST: Disciplina/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DisciplinaViewModels disciplinaViewModels = db.Disciplina.Find(id);
            db.Disciplina.Remove(disciplinaViewModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
