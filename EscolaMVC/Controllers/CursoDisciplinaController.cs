﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EscolaMVC.Models;

namespace EscolaMVC.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class CursoDisciplinaController : Controller
    {
        private MyContext db = new MyContext();

        // GET: CursoDisciplina
        public ActionResult Index()
        {
            var cursoDisciplina = db.CursoDisciplina.Include(c => c.Curso).Include(c => c.Disciplina);
            return View(cursoDisciplina.ToList());
        }

        // GET: CursoDisciplina/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CursoDisciplinaViewModels cursoDisciplinaViewModels = db.CursoDisciplina.Find(id);
            if (cursoDisciplinaViewModels == null)
            {
                return HttpNotFound();
            }
            return View(cursoDisciplinaViewModels);
        }

        // GET: CursoDisciplina/Create
        public ActionResult Create(int? id)
        {
            ViewBag.CursoId = new SelectList(db.Curso.Where(x => x.CursoId == id), "CursoId", "NomeCurso");
            ViewBag.DisciplinaId = new SelectList(db.Disciplina, "DisciplinaId", "NomeDisciplina");
            return View();
        }

        // POST: CursoDisciplina/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CursoDisciplinaId,CursoId,DisciplinaId")] CursoDisciplinaViewModels cursoDisciplinaViewModels)
        {
            if (ModelState.IsValid)
            {
                db.CursoDisciplina.Add(cursoDisciplinaViewModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso", cursoDisciplinaViewModels.CursoId);
            ViewBag.DisciplinaId = new SelectList(db.Disciplina, "DisciplinaId", "NomeDisciplina", cursoDisciplinaViewModels.DisciplinaId);
            return View(cursoDisciplinaViewModels);
        }

        // GET: CursoDisciplina/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CursoDisciplinaViewModels cursoDisciplinaViewModels = db.CursoDisciplina.Find(id);
            if (cursoDisciplinaViewModels == null)
            {
                return HttpNotFound();
            }
            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso", cursoDisciplinaViewModels.CursoId);
            ViewBag.DisciplinaId = new SelectList(db.Disciplina, "DisciplinaId", "NomeDisciplina", cursoDisciplinaViewModels.DisciplinaId);
            return View(cursoDisciplinaViewModels);
        }

        // POST: CursoDisciplina/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CursoDisciplinaId,CursoId,DisciplinaId")] CursoDisciplinaViewModels cursoDisciplinaViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cursoDisciplinaViewModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso", cursoDisciplinaViewModels.CursoId);
            ViewBag.DisciplinaId = new SelectList(db.Disciplina, "DisciplinaId", "NomeDisciplina", cursoDisciplinaViewModels.DisciplinaId);
            return View(cursoDisciplinaViewModels);
        }

        // GET: CursoDisciplina/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CursoDisciplinaViewModels cursoDisciplinaViewModels = db.CursoDisciplina.Find(id);
            if (cursoDisciplinaViewModels == null)
            {
                return HttpNotFound();
            }
            return View(cursoDisciplinaViewModels);
        }

        // POST: CursoDisciplina/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CursoDisciplinaViewModels cursoDisciplinaViewModels = db.CursoDisciplina.Find(id);
            db.CursoDisciplina.Remove(cursoDisciplinaViewModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
