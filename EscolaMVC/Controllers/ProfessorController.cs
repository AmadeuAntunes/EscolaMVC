﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EscolaMVC.Models;

namespace EscolaMVC.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class ProfessorController : Controller
    {
        private MyContext db = new MyContext();

        // GET: Utilizador
        public ActionResult Index()
        {
            var utilizador = db.Utilizador.Include(u => u.Cargo);

            var professores =
            from prof in utilizador
            where prof.Cargo.CargoNome == "Professor"
            select prof;



            
            return View(professores.ToList());
        }

        // GET: Utilizador/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UtilizadorViewModels utilizadorViewModels = db.Utilizador.Find(id);
            if (utilizadorViewModels == null)
            {
                return HttpNotFound();
            }
            return View(utilizadorViewModels);
        }

        // GET: Utilizador/Create
        public ActionResult Create()
        {
            ViewBag.CargoId = new SelectList(db.Cargo, "CargoId", "CargoNome");
            return View();
        }

        // POST: Utilizador/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RegistoId,Email,Nome,Apelido,Password,CargoId")] UtilizadorViewModels utilizadorViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Utilizador.Add(utilizadorViewModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CargoId = new SelectList(db.Cargo, "CargoId", "CargoNome", utilizadorViewModels.CargoId);
            return View(utilizadorViewModels);
        }

        // GET: Utilizador/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UtilizadorViewModels utilizadorViewModels = db.Utilizador.Find(id);
            if (utilizadorViewModels == null)
            {
                return HttpNotFound();
            }
            ViewBag.CargoId = new SelectList(db.Cargo, "CargoId", "CargoNome", utilizadorViewModels.CargoId);
            return View(utilizadorViewModels);
        }

        // POST: Utilizador/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UtilizadorId,Email,Nome,Apelido,CargoId")] UtilizadorViewModels utilizadorViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(utilizadorViewModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CargoId = new SelectList(db.Cargo, "CargoId", "CargoNome", utilizadorViewModels.CargoId);
            return View(utilizadorViewModels);
        }

        // GET: Utilizador/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UtilizadorViewModels utilizadorViewModels = db.Utilizador.Find(id);
            if (utilizadorViewModels == null)
            {
                return HttpNotFound();
            }
            return View(utilizadorViewModels);
        }

        // POST: Utilizador/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UtilizadorViewModels utilizadorViewModels = db.Utilizador.Find(id);
            db.Utilizador.Remove(utilizadorViewModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
