﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace EscolaMVC.Models
{
    [Authorize(Roles = "Administrador")]
    public class TurmaController : Controller
    {
        private MyContext db = new MyContext();
        [Authorize]
        // GET: Turma
        public ActionResult Index(int? id)
        {
           if(id == null)
            {
                var turma = db.Turma.Include(t => t.Curso);
                return View(turma.ToList());
            }
            else
            {
                var Turma = from x in db.Turma.Include(t => t.Curso)
                            where x.CursoId == id
                            select x;


                return View(Turma.ToList());
            }

        }

       
        // GET: Turma/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TurmaViewModels turmaViewModels = db.Turma.Find(id);
            var turmas = db.Turma.Include(t => t.Curso).ToList();

             var Turma = from x in db.Turma.Include(t => t.Curso)
                     where x.TurmaId == id
                     select x;

            if (turmaViewModels == null)
            {
                return HttpNotFound();
            }
            return View(Turma.FirstOrDefault());
        }

        // GET: Turma/Create
        public ActionResult Create()
        {
            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso");
            return View();
        }

        // POST: Turma/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TurmaId,NomeTurma,CursoId")] TurmaViewModels turmaViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Turma.Add(turmaViewModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso", turmaViewModels.CursoId);
            return View(turmaViewModels);
        }

        // GET: Turma/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TurmaViewModels turmaViewModels = db.Turma.Find(id);
            if (turmaViewModels == null)
            {
                return HttpNotFound();
            }
            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso", turmaViewModels.CursoId);
            return View(turmaViewModels);
        }

        // POST: Turma/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TurmaId,NomeTurma,CursoId")] TurmaViewModels turmaViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(turmaViewModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso", turmaViewModels.CursoId);
            return View(turmaViewModels);
        }

        // GET: Turma/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TurmaViewModels turmaViewModels = db.Turma.Find(id);
            var turmas = db.Turma.Include(t => t.Curso).ToList();

            var Turma = from x in db.Turma.Include(t => t.Curso)
                        where x.TurmaId == id
                        select x;

            if (turmaViewModels == null)
            {
                return HttpNotFound();
            }
            return View(Turma.FirstOrDefault());
        }

        // POST: Turma/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TurmaViewModels turmaViewModels = db.Turma.Find(id);
            db.Turma.Remove(turmaViewModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
