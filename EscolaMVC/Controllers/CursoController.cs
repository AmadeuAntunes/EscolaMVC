﻿namespace EscolaMVC.Controllers
{
    using EscolaMVC.Models;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;
    [Authorize(Roles = "Administrador")]
    public class CursoController : Controller
    {
        private MyContext db = new MyContext();

        // GET: Curso
        public ActionResult Index()
        {
            return View(db.Curso.ToList());
        }

        // GET: Curso/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CursoViewModels cursoViewModels = db.Curso.Find(id);
            if (cursoViewModels == null)
            {
                return HttpNotFound();
            }
            return View(cursoViewModels);
        }

        // GET: Curso/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Curso/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CursoId,NomeCurso")] CursoViewModels cursoViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Curso.Add(cursoViewModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cursoViewModels);
        }

        // GET: Curso/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CursoViewModels cursoViewModels = db.Curso.Find(id);
            if (cursoViewModels == null)
            {
                return HttpNotFound();
            }
            return View(cursoViewModels);
        }

        // POST: Curso/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CursoId,NomeCurso")] CursoViewModels cursoViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cursoViewModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cursoViewModels);
        }

        // GET: Curso/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CursoViewModels cursoViewModels = db.Curso.Find(id);
            if (cursoViewModels == null)
            {
                return HttpNotFound();
            }
            return View(cursoViewModels);
        }

        // POST: Curso/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CursoViewModels cursoViewModels = db.Curso.Find(id);
            db.Curso.Remove(cursoViewModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
