﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using EscolaMVC.Models;

namespace EscolaMVC.Controllers
{
    [Authorize(Roles = "Administrador,Professor")]
    
    public class NotaController : Controller
    {
        private MyContext db = new MyContext();

        // GET: Nota
        public ActionResult Index()
        {
            var nota = db.Nota.Include(n => n.Aluno).Include(n => n.Curso).Include(n => n.Disciplina).Include(n => n.Turma);
            return View(nota.ToList());
        }

        // GET: Nota/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NotaViewModels notaViewModels = db.Nota.Find(id);
            if (notaViewModels == null)
            {
                return HttpNotFound();
            }
            return View(notaViewModels);
        }

        // GET: Nota/Create
        public ActionResult Create()
        {
            ViewBag.UtilizadorId = new SelectList(db.Utilizador.Where(x => x.CargoId == 3), "UtilizadorId", "Email");
            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso");
            ViewBag.DisciplinaId = new SelectList(db.Disciplina, "DisciplinaId", "NomeDisciplina");
            ViewBag.TurmaId = new SelectList(db.Turma, "TurmaId", "NomeTurma");
            return View();
        }

        // POST: Nota/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NotaId,CursoId,TurmaId,DisciplinaId,UtilizadorId,NotaValor")] NotaViewModels notaViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Nota.Add(notaViewModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UtilizadorId = new SelectList(db.Utilizador, "UtilizadorId", "Email", notaViewModels.UtilizadorId);
            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso", notaViewModels.CursoId);
            ViewBag.DisciplinaId = new SelectList(db.Disciplina, "DisciplinaId", "NomeDisciplina", notaViewModels.DisciplinaId);
            ViewBag.TurmaId = new SelectList(db.Turma, "TurmaId", "NomeTurma", notaViewModels.TurmaId);
            return View(notaViewModels);
        }

        // GET: Nota/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NotaViewModels notaViewModels = db.Nota.Find(id);
            if (notaViewModels == null)
            {
                return HttpNotFound();
            }
            ViewBag.UtilizadorId = new SelectList(db.Utilizador, "UtilizadorId", "Email", notaViewModels.UtilizadorId);
            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso", notaViewModels.CursoId);
            ViewBag.DisciplinaId = new SelectList(db.Disciplina, "DisciplinaId", "NomeDisciplina", notaViewModels.DisciplinaId);
            ViewBag.TurmaId = new SelectList(db.Turma, "TurmaId", "NomeTurma", notaViewModels.TurmaId);
            return View(notaViewModels);
        }

        // POST: Nota/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NotaId,CursoId,TurmaId,DisciplinaId,UtilizadorId,NotaValor")] NotaViewModels notaViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(notaViewModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UtilizadorId = new SelectList(db.Utilizador, "UtilizadorId", "Email", notaViewModels.UtilizadorId);
            ViewBag.CursoId = new SelectList(db.Curso, "CursoId", "NomeCurso", notaViewModels.CursoId);
            ViewBag.DisciplinaId = new SelectList(db.Disciplina, "DisciplinaId", "NomeDisciplina", notaViewModels.DisciplinaId);
            ViewBag.TurmaId = new SelectList(db.Turma, "TurmaId", "NomeTurma", notaViewModels.TurmaId);
            return View(notaViewModels);
        }

        // GET: Nota/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NotaViewModels notaViewModels = db.Nota.Find(id);
            if (notaViewModels == null)
            {
                return HttpNotFound();
            }
            return View(notaViewModels);
        }

        // POST: Nota/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NotaViewModels notaViewModels = db.Nota.Find(id);
            db.Nota.Remove(notaViewModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
