﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EscolaMVC.Models;

namespace EscolaMVC.Controllers
{
    [Authorize (Roles = "Administrador")]
    public class CargoController : Controller
    {
     
        private MyContext db = new MyContext();

        // GET: Cargo
        public ActionResult Index()
        {
            return View(db.Cargo.ToList());
        }

        // GET: Cargo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CargoViewModels cargoViewModels = db.Cargo.Find(id);
            if (cargoViewModels == null)
            {
                return HttpNotFound();
            }
            return View(cargoViewModels);
        }

        // GET: Cargo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cargo/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CargoId,CargoNome")] CargoViewModels cargoViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Cargo.Add(cargoViewModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cargoViewModels);
        }

        // GET: Cargo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CargoViewModels cargoViewModels = db.Cargo.Find(id);
            if (cargoViewModels == null)
            {
                return HttpNotFound();
            }
            return View(cargoViewModels);
        }

        // POST: Cargo/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CargoId,CargoNome")] CargoViewModels cargoViewModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cargoViewModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cargoViewModels);
        }

        // GET: Cargo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CargoViewModels cargoViewModels = db.Cargo.Find(id);
            if (cargoViewModels == null)
            {
                return HttpNotFound();
            }
            return View(cargoViewModels);
        }

        // POST: Cargo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CargoViewModels cargoViewModels = db.Cargo.Find(id);
            db.Cargo.Remove(cargoViewModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
