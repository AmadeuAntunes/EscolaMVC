﻿namespace EscolaMVC.Controllers
{
    using EscolaMVC.Models;
    using System.Linq;
    using System.Web.Mvc;
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private MyContext db = new MyContext();
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}